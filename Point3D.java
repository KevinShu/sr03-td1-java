import java.util.Scanner;

public class Point3D extends Point2D {
    private double z;

    public Point3D(){
        this.x = 0.0;
        this.y = 0.0;
        this.z = 0.0;
    }

    public Point3D(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getZ(){
        return this.z;
    }

    public void setZ(double z){
        this.z = z;
    }

    public void print(){
        System.out.println("(" + this.x + ", " + this.y + ", " + this.z + ")");
    }

    public double calculerDistance(Point3D point2){
        return Math.sqrt(Math.pow(Math.abs(this.x - point2.x),2) + Math.pow(Math.abs(this.y - point2.y), 2) + Math.pow(Math.abs(this.z - point2.z), 2));
    }

    public String toString(){
        return "(" + this.x + ", " + this.y + ", " + this.z + ")";
    }

    public static void main(String[] args) {
        Point3D p1 = new Point3D(-5, -7, 8);
        Point3D p2 = new Point3D(-8, 6, -2); 
        System.out.println("distance p1 p2 : " + p1.calculerDistance(p2));

        Point3D tableau[] = new Point3D[3];
        try (Scanner sc = new Scanner(System.in)) {
            for (int i = 0; i < 3; i++){
                System.out.println("Veulliez saisir x, y et z : ");
                double inputX = sc.nextDouble();
                double inputY = sc.nextDouble();
                double inputZ = sc.nextDouble();
                System.out.println("vous avez saisir : " + "(" + inputX + ", " + inputY + ", " + inputZ + ")\n");
                tableau[i] = new Point3D(inputX, inputY, inputZ);
            }
        }
        for (int i=0; i<3; i++){
            System.out.println("Les points : \n");
            tableau[i].print();
        }
    }
}
