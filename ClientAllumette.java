import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientAllumette {
    public static void main(String[] args) {
        int intmsg;
        byte b[] = new byte[200];
        try {
            Socket client = new Socket ("localhost", 10010);
            OutputStream out = client.getOutputStream();
            InputStream in = client.getInputStream();
            Scanner sc = new Scanner(System.in);
            String chaine;
            while(true){
                in.read(b);
                chaine = new String(b);
                System.out.println("[Msg serveur] " + chaine);
                if (chaine.startsWith("GAMEOVER")){
                    break;
                }
                System.out.printf("Entrez votre réponse: ");
                intmsg = sc.nextInt();
                System.out.printf("\n");
                out.write(Integer.toString(intmsg).getBytes());
                b = new byte[200];
            }
            in.close();
            out.close();
            client.close();
            // attendre 20ms pour que l'ecriture soit fini avant de fermer
            try { 
                Thread.sleep(20);
            } catch (InterruptedException ex) { 
                Logger.getLogger(ClientExemple.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) { 
            Logger.getLogger(ClientExemple.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
