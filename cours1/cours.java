package cours1;
// utilisation de package (dans repertoire cours1)

public class cours {
    public static void main(String[] args) {

        for(String arg : args){
            System.out.println("Argument received : " + arg);
        }

        //declaration
        int acc = 0;

        int[][] table = new int[5][];
        for(int i=0; i<table.length; i++){
            table[i] = new int[10];
        }

        System.out.println("Table length : " + table.length);


        //affectation
        for(int i=0; i<table.length; i++){
            for (int j=0; j<table[0].length; j++){
                table[i][j] = 10086;
                acc++;
            }
        }

        //affichage
        for(int[] line : table){
            for(int rowofline : line){
                System.out.println(rowofline);
            }
        }

        System.out.println("Total elements : " + acc + "(" + table.length*table[0].length + ")");
    }

    exempleclass hello = new exempleclass(3, 4);
}
