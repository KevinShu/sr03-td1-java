package cours1;

import java.io.*;

// public class Adress {
//     int noRue;
//     String typeRue;
//     String nomRue;
//     String ville;
//     int codePostal;
//     String pays;
// }

// public class Proprietaire {
//     String nom;
//     String prenom;
    
// }

public class Voiture implements java.io.Serializable{
    static int nbVoiture;

    protected String marque;
    protected String plaque;
    protected String immatriculation;
    protected String couleur;

    

    public Voiture(){
        this.marque = "";
        this.plaque = "";
        this.immatriculation = "";
        this.couleur = "";
        Voiture.nbVoiture++;
    }

    public Voiture(String marque, String plaque, String immatriculation){
        this.marque = marque;
        this.plaque = plaque;
        this.immatriculation = immatriculation;
        Voiture.nbVoiture++;
    }

    public Voiture(String marque, String plaque, String immatriculation, String couleur){
        this.marque = marque;
        this.plaque = plaque;
        this.immatriculation = immatriculation;
        this.couleur = couleur;
        Voiture.nbVoiture++;
    }

    public String getMarque(){
        return this.marque;
    }

    public void setMarque(String marque){
        this.marque = marque;
    }

    public static void main(String args[]){
        Voiture v1 = new Voiture("Reno", "Clio", "12345");
        Voiture v2 = new Voiture("Tesla", "T1", "54321", "Noir");
        Voiture v3 = new Voiture();
        Voiture v4 = new Voiture();

        try {
            // create a new file with an ObjectOutputStream
            // object -> object output stream -> file output stream -> file
            FileOutputStream fout = new FileOutputStream("v_output.txt");
            ObjectOutputStream oout = new ObjectOutputStream(fout);
   
            // write something in the file
            oout.writeObject(v1);
            oout.writeObject(v2);
   
            // close the stream
            oout.close();
   
            // create an ObjectInputStream for the file we created before
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("v_output.txt"));
   

            // read and print what we wrote before
            // System.out.println("" + (String) ois.readObject());
            // System.out.println("" + ois.readObject());

            // allocation to new object using a cast from the inputstream
            // file -> file input stream -> object input stream -> object
            v3 = (Voiture) ois.readObject();
            v4 = (Voiture) ois.readObject();

            System.out.println(v3.marque + v3.plaque + v3.immatriculation + v3.couleur);
            System.out.println(v4.marque + v4.plaque + v4.immatriculation + v4.couleur);

         } catch (Exception ex) {
            ex.printStackTrace();
         }

    }
}
