import java.util.Scanner;

public class Prog2 {
    public static void main(String[] args) {
        System.out.println("Veuillez entrer 10 nombre ......\n");
        // initialisation avec new
        int[] nombres = new int[10]; // allocation de reference de tableau a nombre
        try (Scanner sc = new Scanner(System.in)) {
            for (int i = 0; i < 10; i++){
                System.out.println("Veulliez saisir un nombre : ");
                int str = sc.nextInt();
                System.out.println("vous avez saisir le nombre : " + str + "\n");
                nombres[i] = str;
            }
        }
        System.out.println("Le tableau : ");
        for (int i = 0; i < 10; i++){
            System.out.printf("%d ", nombres[i]);
        }

        System.out.println("Maximal du tableau : " + max(nombres, 10));
        System.out.println("Minimal du tableau : " + min(nombres, 10));
        System.out.println("Ecart-type du tableau : " + s(nombres, 10));
        System.out.println("Variance du tableau : " + s2(nombres, 10));
    }

    private static int min(int[] nombre, int len){
        int min_nombre = nombre[0];
        for(int i = 1; i < len; i++){
            if (nombre[i] < min_nombre){
                min_nombre = nombre[i];
            }
        }
        return min_nombre;
    }

    private static int max(int[] nombre, int len){
        int max_nombre = nombre[0];
        for(int i = 1; i < len; i++){
            if (nombre[i] > max_nombre){
                max_nombre = nombre[i];
            }
        }
        return max_nombre;
    }

    private static double s(int[] nombre, int len){
        double result = 0;

        return result;
    }

    private static double s2(int[] nombre, int len){
        double result = 0;

        return result;
    }
}
