import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Serveur {

    public static void main(String[] args) {
        try {
            System.out.println("[Serveur] Attente de connexion.");
            ServerSocket conn = new ServerSocket(10086);
            while(true) {
                Socket comm = conn.accept();
                if (comm.isConnected()){
                    System.out.println("[Serveur] Connecxion établie...");
                }
            }
        } catch (IOException e) {
            // TODO Auto-generated catch block
            Logger.getLogger(ServerSocket.class.getName()).log(Level.SEVERE, null, e);
        }
        
    }
}
