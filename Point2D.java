import java.util.Scanner;

public class Point2D {
    protected double x;
    protected double y;

    public Point2D(){
        this.x = 0.0;
        this.y = 0.0;
    }

    public Point2D(double x, double y){
        this.x = x;
        this.y = y;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }
    
    public void setX(double x){
        this.x = x;
    }

    public void setY(double y){
        this.y = y;
    }

    public double calculerDistance(Point2D point2){
        return Math.sqrt(Math.pow(Math.abs(this.x - point2.x),2) + Math.pow(Math.abs(this.y - point2.y), 2));
    }

    public void print(){
        System.out.println("(" + this.x + ", " + this.y + ")");
    }

    public String toString(){
        return "(" + this.x + ", " + this.y + ")";
    }

    public static void main(String[] args) {
        Point2D p1 = new Point2D(-5, -7);
        Point2D p2 = new Point2D(-8, 6);
        System.out.println("distance p1 p2 : " + p1.calculerDistance(p2));

        Point2D tableau[] = new Point2D[5];
        try (Scanner sc = new Scanner(System.in)) {
            for (int i = 0; i < 5; i++){
                System.out.println("Veulliez saisir x et y : ");
                double inputX = sc.nextDouble();
                double inputY = sc.nextDouble();
                System.out.println("vous avez saisir : " + "(" + inputX + ", " + inputY + ")\n");
                tableau[i] = new Point2D(inputX, inputY);
            }
        }
        for (int i=0; i<5; i++){
            System.out.println("Les points : \n");
            tableau[i].print();
        }
    }
}


