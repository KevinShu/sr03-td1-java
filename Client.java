import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client {
    public static void main(String[] args) {
        try {
            System.out.println("[Client] Se connecter au serveur 10086.");
            Socket conn = new Socket("localhost", 10086);
            if (conn.isConnected()){
                System.out.println("[Client] Connecxion établie.");
            }
            conn.close();
        } catch (IOException e) {
            System.out.println("[Client] Connecxion échouée.");
            Logger.getLogger(ServerSocket.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
