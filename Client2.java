import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Client2 {
    public static void main(String[] args) {
        try {
            System.out.println("[Client] Se connecter au serveur 10086.");
            Socket conn = new Socket("localhost", 10086);
            if (conn.isConnected()){
                System.out.println("[Client] Connecxion établie.");
            }

            OutputStream out = conn.getOutputStream();
            out.write("Bonjour serveur".getBytes());

            InputStream in = conn.getInputStream();
            byte []b = new byte[1024];
            in.read(b);
            System.out.println("le serveur a répondu: " + new String(b));

            // out.close();
            // in.close();
            // conn.close();

        } catch (IOException e) {
            System.out.println("[Client] Connecxion échouée.");
            Logger.getLogger(ServerSocket.class.getName()).log(Level.SEVERE, null, e);
        }
    }
}
