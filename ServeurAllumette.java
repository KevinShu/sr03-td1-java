import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServeurAllumette {
    public static int jeu_ordi (int nb_allum, int prise_max)
    {
        // ex: 20, 4
        int prise = 0;
        int s = 0;
        float t = 0;
        s = prise_max + 1; // s = 5
        t = ((float) (nb_allum - s)) / (prise_max + 1); // 20 - S / 5
        while (t != Math.floor(t))
        {
            s--; // diminuer s jusqu'a a division soit entiers
            t = ((float) (nb_allum-s)) / (prise_max + 1); // 20 - S / 5
        }
        prise = s - 1;
        if (prise == 0)
            prise = 1;
        return prise;
    }
    public static String afficher_allumettes(int n)
    {
        String affichage="";
        affichage += "\n";
        affichage += "\n";
        for (int i=0; i<n; i++)
        {
            affichage += "o";
        }
        affichage += "\n";
        for (int i=0; i<n; i++)
        {
            affichage += "|";
        }
        affichage += "\n";
        affichage += "\n";

        return affichage;
    }
    public static void main(String[] args) {
        int nb_allumette, max_pick, who_start, nb_rest;
        boolean valid_response = false;
        try {
            ServerSocket conn = new ServerSocket(10010);
            Socket comm = conn.accept();
            OutputStream out = comm.getOutputStream();
            InputStream in = comm.getInputStream();
            byte b[] = new byte[200];
            String chaine;
            out.write("Bienvenue au jeu d'allumette, nb allumettes = ?".getBytes());
            do{
                valid_response = false;
                in.read(b);
                chaine = new String(b);
                chaine = chaine.trim();
                nb_allumette = Integer.parseInt(chaine, 10);
                if (nb_allumette>=10 && nb_allumette<=60){
                    System.out.println("nb allumettes = " + nb_allumette);
                    valid_response = true;
                } else {
                    out.write("Réponse invalide, nb allumettes = ?".getBytes());
                }
                b = new byte[200];
            }while(!valid_response);
            
            out.write("nb max d'allumettes que l'on peut retirer = ?".getBytes());
            do{
                valid_response = false;
                in.read(b);
                chaine = new String(b);
                chaine = chaine.trim();
                max_pick = Integer.parseInt(chaine, 10);
                if (max_pick>0 && max_pick<nb_allumette){
                    System.out.println("nb max pick = " + max_pick);
                    valid_response = true;
                } else {
                    out.write("Réponse invalide, nb max d'allumettes que l'on peut retirer = ?".getBytes());
                }
                b = new byte[200];
            }while(!valid_response);

            out.write("Joueur commence (0: user, 1: ordi) = ?".getBytes());
            do{
                valid_response = false;
                in.read(b);
                chaine = new String(b);
                chaine = chaine.trim();
                who_start = Integer.parseInt(chaine, 10);
                if (who_start==0 || who_start==1){
                    System.out.println("Joueur commence = " + (who_start==1?"ordi":"user") );
                    valid_response = true;
                } else {
                    out.write("Réponse invalide, Joueur commence (0: user, 1: ordi) = ?".getBytes());
                }
                b = new byte[200];
            }while(!valid_response);

            nb_rest = nb_allumette;
            String resp = "";
            while(nb_rest != 0)
            {
                int prise;
                // afficher_allumettes(nb_allu_rest);
                switch(who_start)
                {
                case 0:
                    resp += "Il reste " + nb_rest + " allumettes" + '\n';
                    resp += afficher_allumettes(nb_rest);
                    resp += "Le nombre d'allumettes vous voulez retirez ? ";
                    out.write(resp.getBytes());
                    do{
                        valid_response = false;
                        in.read(b);
                        chaine = new String(b);
                        chaine = chaine.trim();
                        prise = Integer.parseInt(chaine, 10);
                        if (prise>=1 && prise<=nb_rest && prise<=max_pick){
                            valid_response = true;
                            nb_rest-=prise;
                            System.out.println("user pris " + prise);
                        } else {
                            out.write("Réponse invalide, réessayer, combien ?".getBytes());
                        }
                        b = new byte[200];
                    }while(!valid_response);
                    resp = "";
                    break;
                case 1:
                    prise = jeu_ordi(nb_rest,max_pick);
                    resp += "L'ordi a retiré " + prise + " allumettes.\n";
                    System.out.println("system pris " + prise);
                    nb_rest -= prise;
                    break;
                default:
                    break;
                }

                who_start = (who_start+1)%2;
            }

            System.out.println("Fin");
            if (who_start == 1)
            {
                out.write("GAMEOVER, COMPUTER WIN".getBytes());
            }
            else
            {
                out.write("GAMEOVER, PLAYER WIN".getBytes());
            }
            in.close();
            out.close();
            comm.close();
        } catch (IOException ex) {
            Logger.getLogger(ServerSocket.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
