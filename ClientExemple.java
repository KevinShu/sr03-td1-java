import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ClientExemple {
    public static void main(String[] args) {
        int compteur = 0, nbMax;
        byte b[] = new byte[20];
        try {
            Socket client = new Socket ("localhost", 10080);
            OutputStream out = client.getOutputStream();
            InputStream in = client.getInputStream();
            out.write("bonjour4".getBytes());
            System.out.println("Donnez le nombre max de messages à envoyer au serveur :");
            Scanner sc = new Scanner(System.in);
            nbMax = sc.nextInt();
            while(compteur != nbMax){
                in.read(b);
                System.out.println("le serveur a dit: " + new String(b));
                System.out.println("Le client envoie le message:message"+compteur);
                out.write(("message"+compteur).getBytes());
                compteur++;
                b = new byte[20];
                try { 
                Thread.sleep(1000);
                } catch (InterruptedException ex) { 
                    Logger.getLogger(ClientExemple.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            out.write(("END").getBytes());
            in.close();
            out.close();
            client.close();
            // attendre 20ms pour que l'ecriture soit fini avant de fermer
            try { 
                Thread.sleep(20);
            } catch (InterruptedException ex) { 
                Logger.getLogger(ClientExemple.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (IOException ex) { 
            Logger.getLogger(ClientExemple.class.getName()).log(Level.SEVERE, null, ex);
        } 
    }
}
